public class Rat{
	public String name;
	public String favorFood;
	public double age;
	
	public void eat(){
		System.out.println(this.name+" is eating "+this.favorFood+". "+this.name+" like "+this.favorFood+" so much!");
	}
	public void sleep(){
		System.out.println(this.name+": ZZZZzzzz");
	}
}