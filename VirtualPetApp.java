import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[]args){
		Rat[] mischief=new Rat[4];
		Scanner reader=new Scanner(System.in);
		for(int i=0; i<mischief.length; i++){
			mischief[i]=new Rat();
			System.out.println("Choose a name for your rat "+(i+1) );
			mischief[i].name=reader.next();
			System.out.println("Choose the favorite food for your rat "+(i+1) );
			mischief[i].favorFood=reader.next();
			System.out.println("Choose the age(no more than 4 year) for your rat "+(i+1) );
			mischief[i].age=reader.nextDouble();
			
		}
		
		System.out.println(mischief[mischief.length-1].name);
		System.out.println(mischief[mischief.length-1].favorFood);
		System.out.println(mischief[mischief.length-1].age);
		
		mischief[0].eat();
		mischief[0].sleep();
	}
}